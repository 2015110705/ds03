#pragma once 

#define SWAP(X, Y, T) ((T) = (X), (X) = (Y), (Y) = (T))

void sort(int _ary[], int _step)
{
	/* selection sort */
	int nMin = 0;
	int i, j;
	int temp;

	for (i = 0; i < _step; i++) {
		nMin = i;

		/* find minimum value */
		for(j = i; j < _step; j++){
			if(_ary[nMin] > _ary[j]){
				nMin = j;
			}
		}
		
		/* swap */
		temp = _ary[i];
		_ary[i] = _ary[nMin];
		_ary[nMin] = temp;
	}
}